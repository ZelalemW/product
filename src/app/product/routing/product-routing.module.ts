import { NgModule } from '@angular/core';
import { ProductListComponent } from '../list/product-list.component';
import { ProductDetailComponent } from '../detail/product-detail.component';
import { RouterModule } from '@angular/router';
import { ProductDetailGuard } from '../guard/product-detail.guard';

@NgModule({
  imports: [
    RouterModule.forChild([{ path: 'products', component: ProductListComponent },
      {
        path: 'products/:id',
        canActivate: [ProductDetailGuard],
        component: ProductDetailComponent
      }
    ])
  ],
  exports:[
    RouterModule
]})
export class ProductRoutingModule { }
