import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IProduct } from '../../product/model/product.model';
import { ProductService } from '../../product/service/product.service';
// import { EventEmitter } from 'events';

@Component({
  // selector: 'pm-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  pageTitle: string = "Product List";
  imageWidth: number = 50;
  imageMargin: number = 2;
  filteredProducts: IProduct[];
  showImage: boolean = false;
  filterByProdName: string = "cart";
  message: string;
  products: IProduct[];
  errMessage: string;

  notify: EventEmitter<any> = new EventEmitter<any>(); // NOTE - use strongly typed instead.


  _listFilter: string;
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.filterBy(this._listFilter) : this.products;
  }

  filterBy(value: string): IProduct[] {
    let filterLowered = value.toLocaleLowerCase();
    return this.products.filter((prod: IProduct) => prod.productName.toLocaleLowerCase().indexOf(filterLowered) !== -1)
  }

  onFilterChanged(val: string): void {
    let filterLowered = val.toLocaleLowerCase();
    this.filteredProducts = this.products.filter((prod: IProduct) => prod.productName.toLocaleLowerCase().indexOf(filterLowered) !== -1)
  }

  constructor(private productService: ProductService) {
    this._listFilter = 'cart';
  }

  ngOnInit() {
    this.productService.getProducts()
      .subscribe({
        next: products => {
          this.products = products;
          this.filteredProducts = this.products;
        },
        error: err => this.errMessage = err
      });
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
    //this.notify.emit({a:1,b:'str'}); // emitting event and passing String value. You can pass anything you define above for notify variable.
    this.notify.emit('Emitting event from parent'); // emitting event and passing String value. You can pass anything you define above for notify variable.
  }

  onRatingClicked(message: string): void {
    this.message = message;
    this.pageTitle = "Product List: " + this.message;
  }
}
