import { NgModule } from '@angular/core';

import { ProductListComponent } from '../list/product-list.component';
import { ProductDetailComponent } from '../detail/product-detail.component';
import { SharedModule } from '../../shared/shared.module';
import { ProductRoutingModule } from './../routing/product-routing.module';

@NgModule({
  imports: [
    SharedModule,
    ProductRoutingModule
  ],
  declarations: [
    ProductListComponent,
    ProductDetailComponent,
  ]
})
export class ProductModule { }
