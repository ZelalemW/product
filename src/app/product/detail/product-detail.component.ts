import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../product/model/product.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../product/service/product.service';
// import { ProductService } from '../product/product.service';

@Component({
  // selector: 'pm-product-detail', --NO NEED. THIS component won't be nested inside another component.
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
  // providers:[ProductService]
})
export class ProductDetailComponent implements OnInit {
  pageTitle: string = 'Product Detail';
  allProducts: IProduct[] = [];
  product: IProduct;
  errorMessage: string;
  imageWidth: number = 200;
  imageMargin: number = 2;

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router) {
    let id = +this.route.snapshot.paramMap.get('id');

    this.productService.getProducts().subscribe(
      {
        next: products => {
          this.allProducts = products;
          let filteredProds = this.allProducts.filter(p => p.productId === id);
          this.product = filteredProds.length > 0 ? filteredProds[0] : null;
        },
        error: err => { this.errorMessage = err; console.log(err); }
      }
    )
  }

  ngOnInit() {
  }

  onBack(): void {
    this.router.navigate(['/products']);
  }

}
