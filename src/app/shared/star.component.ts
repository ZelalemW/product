import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'pm-star-rating',
    templateUrl: './star.component.html',
    styleUrls: ['./star.component.css']
})
export class StarComponent implements OnInit {
    starWidth: number;
    ratingStarsWidth: number;
    @Input() rating: number = 4;
    @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
    @Input() fromParentEvent: EventEmitter<any>=new EventEmitter<any>();

    constructor() {
        this.ratingStarsWidth = 75;
    }

    ngOnInit() {
        this.starWidth = this.rating / 5 * this.ratingStarsWidth; // NOTE: if rating is 5, then stars width will take the whole width of all stars.
        this.fromParentEvent.subscribe(data => {
            //this is where you apply the filter.
            console.log('Child component received an event from parent component. And data received is: ' + JSON.stringify(data));
        });
    }

    onClick(): void {
        this.ratingClicked.emit(`The rating ${this.rating} was clicked.`);
    }

    onParentClked(dt: string): void {
        console.log('Event received from parent. This is a child playing...');
    }
}